import React, { useEffect } from 'react'
import { OpenInNew } from "@material-ui/icons";
import PropTypes from 'prop-types'
import { Grid, Card, CardContent, CardMedia, Typography, CardActionArea, IconButton, Tooltip } from '@material-ui/core'
import { setSingleImage } from "../redux/singleImage/singeImageSlice";
import { useDispatch, useSelector } from "react-redux";
import anime from 'animejs/lib/anime.es.js';

const style = {
  card: {
    borderRadius: 10,
  },
  media: {
    height: 140,
  },
  content: {
    height: 30,
    backgroundColor: '#00000033'
  },
  iconbutton: {
    marginLeft: 10
  }
}

function Thumbnail(props) {
  const { imageInfo } = props
  const dispatch = useDispatch()
  const filteredImages = useSelector(s => s.filteredImages)
  const {darkmode} = useSelector(s => s.settings)

  useEffect(() => {
    anime({
      targets: document.querySelectorAll('.thumbnail'),
      opacity: [0, 1],
      duration: 250,
      easing: 'linear',
      delay: (e, i, l) => { return i * 50 }
    });
  })

  const handleClick = (id) => {
    dispatch(setSingleImage(filteredImages.filter(image => id === image.id)[0]))
  }

  const handleOpenInNew = (e, id) => {
    e.preventDefault()
    window.open(imageInfo.url_l || imageInfo.url_m || imageInfo.url_z)
  }

  return (
    <Grid item xs={12} sm={6} lg={3} className='thumbnail'>
      <Card style={{...style.card, boxShadow: darkmode 
        ? '20px 20px 60px #121212, -20px -20px 60px #383838'
        : '20px 20px 60px #686868, -20px -20px 60px #ffffff'
      }}>
        <CardActionArea onClick={() => handleClick(imageInfo.id)}>
          <CardMedia style={style.media} image={imageInfo.url_q_cdn} >
            
          </CardMedia>
        </CardActionArea>

        <CardContent style={style.content}>

          <Grid container alignItems='center' >
            <Grid item xs={10}>
              <Typography>
                {imageInfo ? imageInfo.title : null}
              </Typography>
            </Grid>

            <Grid item xs={2}>
              <Tooltip title={`Open in new window`}>
                <IconButton
                  color='primary'
                  onClick={(e) => handleOpenInNew(e, imageInfo.id)}
                  style={style.iconbutton}
                >
                  <OpenInNew />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>

        </CardContent>
      </Card>
    </Grid>
  )
}

Thumbnail.propTypes = {
  imageInfo: PropTypes.object.isRequired
}

export default Thumbnail
