import React from 'react'
import { useDispatch } from "react-redux";
import { Typography, IconButton, Tooltip } from '@material-ui/core'
import { toggleDarkMode } from "../redux/settings/settingsSlice";
import { BrightnessMedium } from "@material-ui/icons";

function Darkmode() {
  const dispatch = useDispatch()

  return (
    <Typography color='textPrimary'>
      <Tooltip title='Toggle Dark Mode'>
        <IconButton onClick={() => dispatch(toggleDarkMode())}>
          <BrightnessMedium color='primary' />
        </IconButton>
      </Tooltip>
    </Typography>
  )
}

export default Darkmode
