import React from 'react'
import { Snackbar, IconButton } from '@material-ui/core'
import { Close } from "@material-ui/icons";
import PropTypes from 'prop-types'
import { Alert } from '@material-ui/lab';

function SnackBar(props) {
  const { handleClose, open} = props

  return (
    <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        action={
          <React.Fragment>
            <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
              <Close fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
        color='primary'
      >
        <Alert onClose={handleClose} severity="success">
          Saved!
        </Alert>
      </Snackbar>
  )
}

SnackBar.propTypes = {
  handleClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired
}

export default SnackBar
