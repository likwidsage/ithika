import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from "react-redux";
import { Grid } from '@material-ui/core'
import { Pagination } from "@material-ui/lab";
import Thumbnail from './Thumbnail'
import { setDisplayedImages } from '../redux/displayedImages/displayedImagesSlice'
import ImageDialog from "./ImageDialog";

const pagStyle = {
  margin: '64px 0 64px 0'
}

function Gallery() {
  const [pagenum, SetPagenum] = useState(1)
  const filteredImages = useSelector(s => s.filteredImages)
  const displayedImages = useSelector(s => s.displayedImages)
  const { perpage } = useSelector(s => s.settings)


  const dispatch = useDispatch()

  const pageChange = (e, num) => {
    SetPagenum(num)
    dispatch(setDisplayedImages(filteredImages.slice((num - 1) * perpage, num * perpage)))
  }

  useEffect(() => {
    SetPagenum(1)
    dispatch(setDisplayedImages(filteredImages.slice(0, perpage)))
  }, [dispatch, filteredImages, perpage])

  const GalleryPagination = () => {
    return (
      <Grid container justify='center' style={pagStyle}>
        <Pagination
          count={Math.ceil(filteredImages.length / perpage)}
          showFirstButton
          showLastButton
          color='primary'
          onChange={pageChange}
          page={pagenum}
          size='small'
        />
      </Grid>

    )
  }

  return (
    <>
      <ImageDialog />
      <Grid container justify='center' >
        <GalleryPagination />
        <Grid container spacing={4} justify='flex-start' style={{ maxWidth: '75%' }}>
          {displayedImages.map(image => (
            <Thumbnail key={image.id} imageInfo={image} />
          ))}
        </Grid>
        <GalleryPagination />
      </Grid>
    </>
  )
}

export default Gallery
