import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from "react-redux";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Button,
  TextField,
  Checkbox,
  IconButton,
  Fade
} from '@material-ui/core'
import { clearSingleImage } from "../redux/singleImage/singeImageSlice";
import { setAllImages } from "../redux/allImages/allImagesSlice";
import { Close } from "@material-ui/icons";
import SnackBar from './SnackBar'
import anime from 'animejs/lib/anime.es.js';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Fade in ref={ref} {...props} />;
})

function ImageDialog() {
  const [imageInfo, setImageInfo] = useState(null)
  const [openSnack, setOpenSnack] = useState(false)
  const allImages = useSelector(s => s.allImages)
  const singleImage = useSelector(s => s.singleImage)
  const dispatch = useDispatch()

  const closeDialog = () => {
    anime({
      targets: '.dialog',
      opacity: [1, 0],
      duration: 100,
      easing: 'linear',
      delay: (e, i, l) => i * 50,
      complete: () => dispatch(clearSingleImage())
    })
  }

  const saveImageInfo = () => {
    let index = allImages.findIndex(image => image.id === imageInfo.id)
    const newAllImages = [...allImages]
    newAllImages[index] = imageInfo
    dispatch(setAllImages(newAllImages))
    dispatch(clearSingleImage())
    setOpenSnack(true)
  }

  useEffect(() => {
    setImageInfo(singleImage)
  }, [singleImage])

  const handleChange = (newValue, field) => {
    const newState = {
      ...imageInfo,
    }

    const fieldArr = field.split('.')
    const children = {}
    if (fieldArr.length > 1) {
      // ['description','_content','a']
      //children.a= newValue
      children[fieldArr[fieldArr.length - 1]] = newValue

      for (let i = fieldArr.length - 2; i > 0; i--) {
        // children._content = { a: newValue}
        children[fieldArr[i]] = { ...children }
        delete children[fieldArr[i + 1]]
      }
      newState[fieldArr[0]] = { ...children }
    } else {
      newState[fieldArr[0]] = newValue
    }

    setImageInfo(newState)
  }

  return (
    <div>
      <SnackBar open={openSnack} handleClose={() => setOpenSnack(false)} />
      <Dialog
        open={imageInfo ? true : false}
        onClose={closeDialog}
        TransitionComponent={Transition}
        className='dialog'
        fullWidth
        maxWidth={'md'}
      >
        <DialogTitle>
          <Grid container>
            <Grid item xs={11}>
              {imageInfo ? imageInfo.title : ''}
            </Grid>
            <Grid item xs={1}>
              <IconButton onClick={closeDialog}>
                <Close color='primary' />
              </IconButton>
            </Grid>
          </Grid>
        </DialogTitle>
        <DialogContent>
          {!imageInfo ? null : (
            <Grid container>

              <Grid item xs={3} >
                Title
              </Grid>
              <Grid item xs={9} >
                <TextField
                  fullWidth
                  label='title'
                  value={imageInfo.title || ''}
                  onChange={e => handleChange(e.target.value, 'title')}
                />
              </Grid>

              <Grid item xs={3} >
                Description
              </Grid>
              <Grid item xs={9} >
                <TextField
                  fullWidth
                  label='Description'
                  value={imageInfo.description._content || ''}
                  onChange={e => handleChange(e.target.value, 'description._content')}
                  multiline
                />
              </Grid>

              <Grid item xs={3} >
                Public
              </Grid>
              <Grid item xs={9} >
                <Checkbox
                  checked={imageInfo.ispublic ? true : false}
                  onChange={e => handleChange(e.target.checked ? 1 : 0, 'ispublic')}
                />
              </Grid>

              <Grid item xs={3} >
                ID
              </Grid>
              <Grid item xs={9} >
                {imageInfo.id || null}
              </Grid>

              <Grid item xs={3} >
                Owner Name
              </Grid>
              <Grid item xs={9} >
                {imageInfo.ownername || null}
              </Grid>

              <Grid item xs={3} >
                Dimension
              </Grid>
              <Grid item xs={9} >
                <div>
                  {imageInfo.width_z && imageInfo.width_z ? `${imageInfo.width_z}x${imageInfo.height_z}` : 'unknown'}
                </div>
              </Grid>

            </Grid>
          )}
        </DialogContent>
        <DialogActions>
          <Button color='secondary' onClick={closeDialog}>Cancel</Button>
          <Button color='primary' onClick={saveImageInfo}>Save</Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default ImageDialog
