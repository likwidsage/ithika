import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import { Typography, Menu, Link, MenuItem } from '@material-ui/core';
import { setFilteredImages } from '../redux/filteredImages/filteredImagesSlice';
import { setPerPage } from '../redux/settings/settingsSlice';

const style ={
  padding: '0 16px'
}

function Search() {
  const [searchText, setSearchText] = useState('')
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [anchorEl, setAnchorEl] = useState(null)
  const allImages = useSelector(s => s.allImages)
  const filteredImages = useSelector(s => s.filteredImages)
  const displayedImages = useSelector(s => s.displayedImages)
  const dispatch = useDispatch()

  const HandleSearchChange = e => {
    setSearchText(e.target.value)
  }

  useEffect(() => {
    dispatch(setFilteredImages(allImages.filter(image=> 
        image.title.match(new RegExp(searchText, 'g')) 
        || (image.description && image.description._content.match(new RegExp(searchText, 'ig')))
      )))
  }, [allImages, dispatch, searchText]) 
  
  const setPerPageMenu = num => {
    HandleMenuClose()
    dispatch(setPerPage(num))
  }

  const PerPageMenu = event => {
    return (
      <Menu
        open={isMenuOpen}
        anchorEl={anchorEl}
        onClose={HandleMenuClose}
      >
        {[4,8,16,32,64,'All'].map(num => {
          return(
            <MenuItem 
              key={num} 
              onClick={() => {setPerPageMenu(num === 'All'? allImages.length : num)}}
            >
              {num}
            </MenuItem>
          )
        })}
      </Menu>
    )
  }

  const HandleMenuOpen = e => {
    e.preventDefault()
    setAnchorEl(e.currentTarget)
    setIsMenuOpen(true)
  }

  const HandleMenuClose = e => {
    setAnchorEl(null)
    setIsMenuOpen(false)
  }

  return (
    <div>
      <PerPageMenu />
      <Grid container justify='center'>
        <Grid item xs={6} md={3} style={style}>
          <TextField 
            fullWidth
            label='Search'
            onChange={HandleSearchChange}
          />
        </Grid>
        <Grid item xs={6} md={3}>
          <Typography color='textPrimary'>
            Displaying{" "}
            {<Link href="" onClick={HandleMenuOpen}>
                {displayedImages.length}
            </Link>} 
            {" "}of {filteredImages.length} images
          </Typography>
        </Grid>
      </Grid>      
    </div>
  )
}

export default Search