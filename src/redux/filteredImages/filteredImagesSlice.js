import { createSlice } from "@reduxjs/toolkit";

const filteredImagesSlice = createSlice({
  name: 'filteredImages',
  initialState: [],
  reducers: {
    setFilteredImages: (state, action) => state = action.payload
  }
})

export const { setFilteredImages } = filteredImagesSlice.actions

export default filteredImagesSlice.reducer