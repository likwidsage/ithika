import {createSlice} from '@reduxjs/toolkit'

export const displayedImagesSlice = createSlice({
  name: 'displayedImages',
  initialState: [],
  reducers: {
    setDisplayedImages: (state, action) => state = action.payload
  }
})

export const { setDisplayedImages } = displayedImagesSlice.actions

export default displayedImagesSlice.reducer