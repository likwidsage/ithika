import { combineReducers } from "redux";
import displayedImages from "../displayedImages/displayedImagesSlice";
import allImages from "../allImages/allImagesSlice";
import filteredImages from "../filteredImages/filteredImagesSlice";
import singleImage from "../singleImage/singeImageSlice";
import settings from "../settings/settingsSlice";

export default combineReducers({
  allImages,
  filteredImages,
  displayedImages,
  singleImage,
  settings
})