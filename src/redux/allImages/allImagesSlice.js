import { createSlice } from "@reduxjs/toolkit";
import data from '../../data.json'

export const allImagesSlice = createSlice({
  name: 'allImages',
  initialState: data.photos.photo,
  reducers: {
    setAllImages(state, action){
      state = action.payload
    }
  }
})

export const { setAllImages } = allImagesSlice.actions

export default allImagesSlice.reducer