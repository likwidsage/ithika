import { createSlice } from "@reduxjs/toolkit";

const settingsSlice = createSlice({
  name: 'settings',
  initialState: {perpage: 8, darkmode: false},
  reducers: {
    toggleDarkMode: (state, action) => { state.darkmode = !state.darkmode },
    setPerPage: (state, action) => { state.perpage = action.payload }
  }
})

export const {toggleDarkMode, setPerPage} = settingsSlice.actions

export default settingsSlice.reducer