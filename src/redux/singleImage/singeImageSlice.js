import { createSlice } from "@reduxjs/toolkit";

const singleImageSlice = createSlice({
  name: 'singleImage',
  initialState: null,
  reducers: {
    setSingleImage: (state, action) => state = action.payload,
    clearSingleImage: (state, action) => state = null,
  }
})

export const {setSingleImage, clearSingleImage} = singleImageSlice.actions

export default singleImageSlice.reducer