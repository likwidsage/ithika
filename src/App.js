import React, { useState, } from 'react';
import galleryApp from './redux/reducers'
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from 'react-redux'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid'
import Search from './components/Search'
import Gallery from './components/Gallery'
import Darkmode from './components/Darkmode'
import anime from 'animejs/lib/anime.es.js';


const store = configureStore({reducer: galleryApp})

const style = {
  sections: { padding: '20px 0' },
  main: { maxWidth: '1920px' }
}

function App() {
  const [theme, setTheme] = useState(createMuiTheme({
    palette: {
      type: 'light',
      primary: {
        main: '#e65100'
      },
      secondary: {
        main: '#0d47a1'
      }
    }
  }))

  let newStore = null
  const storeChangeHandler = () => {
    let prevStore = null
    prevStore = newStore
    newStore = store.getState()
    if (prevStore && newStore) {
      if (newStore.settings.darkmode !== prevStore.settings.darkmode) {
        setTheme(createMuiTheme({
          palette: {
            type: newStore.settings.darkmode ? 'dark' : 'light',
            primary: {
              main: newStore.settings.darkmode ? '#ff9800' : '#e65100'
            },
            secondary: {
              main: newStore.settings.darkmode ? '#2196f3' : '#0d47a1'
            }
          }
        }))
        anime({
          targets: 'body',
          background: newStore.settings.darkmode
            ? '#252525'
            : '#d5d5d5',
          duration: 400,
          easing: 'easeInOutSine',
          delay: (e, i, l) => i * 50,
        })
      }
    }
  }

  store.subscribe(storeChangeHandler)


  return (
    <Provider store={store} >
      <ThemeProvider theme={theme} >
        <Grid container justify='center'>
          <Grid container justify='center' style={style.main}>
            <Grid item xs={1} style={style.sections}>
            </Grid>
            <Grid item xs={10} style={style.sections}>
              <Grid container justify='center'>
                <Typography color='primary' variant='h4'>
                  Image Gallery
                </Typography>
              </Grid>
            </Grid>
            <Grid item xs={1} style={style.sections}>
              <Grid container justify='center'>
                <Darkmode />
              </Grid>
            </Grid>

            <Grid item xs={12} style={style.sections}>
              <Search />
            </Grid>

            <Grid item xs={12} style={style.sections}>
              <Gallery />
            </Grid>

          </Grid>
        </Grid>
      </ThemeProvider>
    </Provider>
  )
}

export default App;
